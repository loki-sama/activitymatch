This is my first reactjs and js single page application. 
It is in an early alpha state, as such major functioality is still missing.
One of my main goals is to learn some major concepts and technologies of js and react.



Example: http://model-chimpanzee-63474.netlify.com/

Backend: https://bitbucket.org/loki-sama/activity-api/

### Technologie used in this app are so far:
  * react - https://reactjs.org/
  * redux - https://redux.js.org/
  * promises 
  * apis and requests - https://www.axios.com/ 
  * redux observable and rxjs - https://redux-observable.js.org/
  * semantic - https://react.semantic-ui.com/introduction
  * immutablejs - https://facebook.github.io/immutable-js/
  * jwt - https://jwt.io/


### backend:

  * expressjs - https://expressjs.com/
  * mongodb and mongoose - http://mongoosejs.com/

### Problems:

  * https doesnt work on backend
  * lot's of functionality is missing
  * testing
  * get some order into code