import { applyMiddleware, createStore } from "redux"

import logger from "redux-logger"
import thunk from "redux-thunk"
import promise from "redux-promise-middleware"
import { composeWithDevTools } from 'redux-devtools-extension';
import Immutable from 'immutable'
import reducers from "./reducers"
import { createEpicMiddleware } from 'redux-observable';
import { rootEpic } from './epics';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/ignoreElements';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/filter';
import 'rxjs/add/observable/of';
import { ajax } from 'rxjs/observable/dom/ajax';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

const epicMiddleware = createEpicMiddleware(rootEpic);

const composeEnhancers = composeWithDevTools({
  serialize: {
    immutable: Immutable
  }
})

const middleware = applyMiddleware(epicMiddleware, promise(), thunk, logger)

export default createStore(reducers, composeEnhancers(middleware))
