import { SEARCH_WIKIPEDIA_PENDING, SEARCH_WIKIPEDIA_FULFILLED, SEARCH_WIKIPEDIA_REJECTED } from '../types'
import { Map, List, fromJS } from 'immutable'
import { Observable } from 'rxjs/Observable';
import ajaxAuth from '../../utils/ajaxAuth'


export default function wikipedia(state = Map({}), action = {}) {
    switch (action.type) {
        case SEARCH_WIKIPEDIA_PENDING: {
            return state
        }
        case SEARCH_WIKIPEDIA_REJECTED: {
            return state
        }
        case SEARCH_WIKIPEDIA_FULFILLED: {
            return fromJS(action.payload)
        }
        default:
            return state;
    }
}

export const wikipediaEpic = (
    action$
) =>
    action$
        .ofType(SEARCH_WIKIPEDIA_PENDING)
        .filter(action => action.payload.length > 2)
        .debounceTime(100)
        .switchMap(action => {
            return ajaxAuth
                .getJSON(`/api/images/wikipedia/page/${action.payload}`)
                .map(response => {
                    return { type: SEARCH_WIKIPEDIA_FULFILLED, payload: response }
                })
                .catch((err) => {
                    console.log("Caught Error, continuing")
                    //Return an empty Observable which gets collapsed in the output
                    return Observable.of({ type: SEARCH_WIKIPEDIA_REJECTED, payload: err });
                });
        });
        // .onErrorResumeNext
        // .subscribe(
        //     (x => console.log('Success', x)),
        //     (x => console.log('Error', x)),
        //     (() => console.log('Complete'))
        //   );
