import { USER_LOGGED_IN, USER_LOGGED_OUT } from "../types";
import { Map, fromJS } from 'immutable'

export default function user(state = Map({}), action = {}) {
  switch (action.type) {
    case USER_LOGGED_IN:
      return fromJS(action.user);
    case USER_LOGGED_OUT:
      return Map({});
    default:
      return state;
  }
}
