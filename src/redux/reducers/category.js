import { FETCH_CATEGORIES_PENDING, FETCH_CATEGORIES_FULFILLED, FETCH_CATEGORIES_REJECTED } from '../types'

import { Map, fromJS } from 'immutable'



function category(state = Map({}), action = {}) {
    switch (action.type) {
        case FETCH_CATEGORIES_FULFILLED: {
            return fromJS(action.payload) }

        default:
            return state;
    }
}

export default category