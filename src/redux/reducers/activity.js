import {
    RANK_ACTIVITY_FULFILLED, RANK_ACTIVITY_PENDING, RANK_ACTIVITY_REJECTED,
    FETCH_ACTIVITIES_FULFILLED, FETCH_ACTIVITIES_PENDING, FETCH_ACTIVITIES_REJECTED,
    FETCH_ACTIVITY_PENDING, FETCH_ACTIVITY_FULFILLED, FETCH_ACTIVITY_REJECTED
} from '../types'
import { Map, List, fromJS } from 'immutable'
import undoable, { ActionTypes } from 'redux-undo-immutable'


function activity(state = Map({}), action = {}) {
    let initialActivity = Map({
        activities: Map({}),
        fetching: false,
        fetched: false,
        error: null
    })
    switch (action.type) {
        case FETCH_ACTIVITIES_PENDING: {
            return state.update(action.rowIndex, initialActivity,
                list => list.set('fetching', true))
        }
        case FETCH_ACTIVITIES_REJECTED: {
            return state.update(action.rowIndex, initialActivity,
                list => list.set('fetching', false).set('error', fromJS(action.payload)))
        }
        case FETCH_ACTIVITIES_FULFILLED: {
            return state.update(action.rowIndex, initialActivity,
                list => list.set('activities', fromJS(action.payload)).set('fetched', true).set('fetching', false))
        }
        case FETCH_ACTIVITY_FULFILLED: {
            return state.set('activity', fromJS(action.payload))
        }
        case RANK_ACTIVITY_FULFILLED: {
            return state.setIn([action.rowIndex, 'activities', action.id.toString(), 'ranks', 0, 'rank'], action.payload.rankRecord.rank)
        }
        case RANK_ACTIVITY_REJECTED: {
            return state
        }

        default:
            return state;
    }
}

const undoableActivity = undoable(activity,
    {
        limit: 0, // set to a number to turn on a limit for the history (0 means infinite)

        // TODO, explain these better:
        // filter for actions that are not allowed to become part of history
        actionFilter: () => true,
        // filter for states that are not allowed to become part of history
        historyFilter: () => true,

        undoType: ActionTypes.UNDO, // define a custom action type for this undo action
        redoType: ActionTypes.REDO, // define a custom action type for this redo action
        jumpType: ActionTypes.JUMP, // define custom action type for this jump action
        jumpToPastType: ActionTypes.JUMP_TO_PAST, // define custom action type for this jumpToPast action
        jumpToFutureType: ActionTypes.JUMP_TO_FUTURE, // define custom action type for this jumpToFuture action
        clearHistoryType: ActionTypes.CLEAR_HISTORY, // define custom action type for this clearHistory action
    })

export default undoableActivity