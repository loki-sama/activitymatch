import { REZISED } from "../types";
import { Map, fromJS } from 'immutable'

export default function activity(state = Map({
    width: window.innerWidth, 
    height: window.innerHeight, 
    quantityItems: Math.floor(window.innerWidth / 300) 
}), action = {}) {
    switch (action.type) {
        case REZISED: {
            return  fromJS(action.payload)
        }
        default:
            return state;
    }
}