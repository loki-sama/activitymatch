import {
    FILE_UPLOAD_FULFILLED
} from '../types'
import { Map, List, fromJS } from 'immutable'


function fileUpload(state = Map({}), action = {}) {
    switch (action.type) {
        case FILE_UPLOAD_FULFILLED: {
            return fromJS(action.payload.response)
        }
        default:
            return state;
    }
}

export default fileUpload