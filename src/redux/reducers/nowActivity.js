import {
    FETCH_ACTIVITY_PENDING, FETCH_ACTIVITY_FULFILLED, FETCH_ACTIVITY_REJECTED
} from '../types'
import { Map, List, fromJS } from 'immutable'
import undoable, { ActionTypes } from 'redux-undo-immutable'


function nowActivity(state = Map({}), action = {}) {
    switch (action.type) {
        case FETCH_ACTIVITY_FULFILLED: {
            return fromJS(action.payload)
        }
        default:
            return state;
    }
}

export default nowActivity