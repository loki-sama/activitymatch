
import { combineReducers } from "redux-immutable"
import user from "./user"
import activity from "./activity"
import layout from './layout'
import image from './image'
import category from './category'
import wikipedia from './wikipedia'
import nowActivity from './nowActivity'
import uploadFile from './uploadFile'




export default combineReducers({
  user,
  activity,
  layout,
  image,
  category,
  wikipedia,
  nowActivity,
  uploadFile
})
