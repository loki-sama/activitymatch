import { SEARCH_IMAGE_PENDING, SEARCH_IMAGE_FULFILLED, SEARCH_IMAGE_REJECTED } from '../types'
import { Map, List, fromJS } from 'immutable'


function image(state = Map({ hits: List([]) }), action = {}) {
    switch (action.type) {
        case SEARCH_IMAGE_PENDING: {
            return state
        }
        case SEARCH_IMAGE_REJECTED: {
            return state
        }
        case SEARCH_IMAGE_FULFILLED: {
            let data = action.payload.hits.map((imageData) => (
                {
                    id: imageData.id,
                    image: imageData.previewURL,
                    tags: imageData.tags,
                    webformatURL: imageData.webformatURL
                }
            ))
            return fromJS(data)
        }
        default:
            return state;
    }
}

export default image