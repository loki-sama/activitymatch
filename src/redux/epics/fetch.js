import { ajax } from 'rxjs/observable/dom/ajax';

const fetchUserFulfilled = payload => ({ type: 'FETCH_USER_FULFILLED', payload });

export default action$ =>
    action$.ofType('FETCH_USER')
        .mergeMap(action =>
            ajax.getJSON(`/api/users/${action.payload}`)
                .map(response => fetchUserFulfilled(response))
                .takeUntil(action$.ofType('FETCH_USER_CANCELLED'))
        );