import { combineEpics } from 'redux-observable';
import { wikipediaEpic } from '../reducers/wikipedia'
import { uploadFileEpic } from '../actions/uploadFileAction'
import { addFeedEpic, addActivityEpic } from '../actions/activityAction'

export const rootEpic = combineEpics(
  wikipediaEpic,
  uploadFileEpic,
  addFeedEpic,
  addActivityEpic
);