import axios from "axios";
import {
  SEARCH_WIKIPEDIA_PENDING, SEARCH_WIKIPEDIA_FULFILLED, SEARCH_WIKIPEDIA_REJECTED,
  SEARCH_IMAGE_PENDING, SEARCH_IMAGE_FULFILLED, SEARCH_IMAGE_REJECTED,
  ADD_ACTIVITY_PENDING, ADD_ACTIVITY_FULFILLED, ADD_ACTIVITY_REJECTED,
  RANK_ACTIVITY_PENDING, RANK_ACTIVITY_FULFILLED, RANK_ACTIVITY_REJECTED,
  FETCH_ACTIVITIES_PENDING, FETCH_ACTIVITIES_FULFILLED, FETCH_ACTIVITIES_REJECTED,
  FETCH_ACTIVITY_PENDING, FETCH_ACTIVITY_FULFILLED, FETCH_ACTIVITY_REJECTED,
  ADD_FEED_PENDING, ADD_FEED_FULFILLED, ADD_FEED_REJECTED,
  ADD_FAVORITE_PENDING, ADD_FAVORITE_FULFILLED, ADD_FAVORITE_REJECTED,
  GET_FAVORITE_PENDING, GET_FAVORITE_FULFILLED, GET_FAVORITE_REJECTED,
  GET_FEED_PENDING, GET_FEED_FULFILLED, GET_FEED_REJECTED,
} from '../types'
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import ajaxAuth from '../../utils/ajaxAuth'
export function fetchActivities(rowIndex, amount, categoryId = null) {
  return function (dispatch) {
    dispatch({ type: FETCH_ACTIVITIES_PENDING, rowIndex: rowIndex });
    console.log(rowIndex);
    axios.post("/api/activities", { amount, category: rowIndex })
      .then((response) => {
        dispatch({ type: FETCH_ACTIVITIES_FULFILLED, payload: response.data, rowIndex: rowIndex })
      })
      .catch((err) => {
        dispatch({ type: FETCH_ACTIVITIES_REJECTED, payload: err, rowIndex: rowIndex })
      })
  }
}

export function fetchActivityById(activityId, ) {
  return function (dispatch) {
    dispatch({ type: FETCH_ACTIVITY_PENDING });
    axios.get("/api/activities/byid/" + activityId)
      .then((response) => {
        dispatch({ type: FETCH_ACTIVITY_FULFILLED, payload: response.data })
      })
      .catch((err) => {
        dispatch({ type: FETCH_ACTIVITY_REJECTED, payload: err })
      })
  }
}

export function rankActivity(rowIndex, id, rank) {
  return function (dispatch) {
    dispatch({ type: RANK_ACTIVITY_PENDING, rowIndex: rowIndex });
    axios.post("/api/activities/userproperty/" + id, { ranks: { rank: rank } })
      .then((response) => {
        dispatch({ type: RANK_ACTIVITY_FULFILLED, payload: response.data, rowIndex: rowIndex, id: id, activityId: id, rank: rank })
      })
      .catch((err) => {
        dispatch({ type: RANK_ACTIVITY_REJECTED, payload: err, rowIndex: rowIndex })
      })
  }
}

export function favActivity(rowIndex, id, fav) {
  return function (dispatch) {
    dispatch({ type: RANK_ACTIVITY_PENDING, rowIndex: rowIndex });

    axios.post("/api/activities/userproperty/" + id, { ranks: { favorite: fav } })
      .then((response) => {
        dispatch({ type: RANK_ACTIVITY_FULFILLED, payload: response.data, rowIndex: rowIndex, id: id, activityId: id, fav: fav })
      })
      .catch((err) => {
        dispatch({ type: RANK_ACTIVITY_REJECTED, payload: err, rowIndex: rowIndex })
      })
  }
}


export function addActivity(data) {
  return function (dispatch) {
    dispatch({ type: ADD_ACTIVITY_PENDING });

    axios.post("/api/activities/add", data)
      .then((response) => {
        dispatch({ type: ADD_ACTIVITY_FULFILLED, payload: response.data })
      })
      .catch((err) => {
        dispatch({ type: ADD_ACTIVITY_REJECTED, payload: err })
      })
  }
}

export function searchImage(data) {
  return function (dispatch) {
    dispatch({ type: SEARCH_IMAGE_PENDING });
    axios
      .get("/api/images/search/" + data)
      .then((response) => {
        dispatch({ type: SEARCH_IMAGE_FULFILLED, payload: response.data })
      })
      .catch((err) => {
        dispatch({ type: SEARCH_IMAGE_REJECTED, payload: err })
      })
  }
}

export function searchWikipedia(data) {
  return function (dispatch) {
    dispatch({ type: SEARCH_WIKIPEDIA_PENDING, payload: data });

    // axios
    //   .get("/api/images/wikipedia/page/" + data)
    //   .then((response) => {
    //     dispatch({ type: SEARCH_WIKIPEDIA_FULFILLED, payload: response.data })
    //   })
    //   .catch((err) => {
    //     dispatch({ type: SEARCH_WIKIPEDIA_REJECTED, payload: err })
    //   })
  }
}


export function updateActivity(id, text) {
  return {
    type: 'UPDATE_ACTIVITY',
    payload: {
      id,
      text,
    },
  }
}

export function deleteActivity(id) {
  return { type: 'DELETE_ACTIVITY', payload: id }
}

export function addFeed(id, { message, resource, kind }) {

  return (dispatch) => {
    dispatch({ type: ADD_FEED_PENDING, activityId: id, payload: { message, resource, kind } });
  }
}

export const addFeedEpic = (
  action$
) =>
  action$
    .ofType(ADD_FEED_PENDING)
    .debounceTime(100)
    .switchMap(action => {
      return ajaxAuth.post(`/api/activities/feed/${action.activityId}`, action.payload, { 'Content-Type': 'application/json' })
        .map(response => {
          return { type: ADD_FEED_FULFILLED, payload: response, activityId: action.activityId }
        })
        .catch((err) => {
          console.log("Caught Error, continuing")
          console.log(err)

          //Return an empty Observable which gets collapsed in the output
          return Observable.of({ type: ADD_FEED_REJECTED, payload: err });
        });
    });

export const addActivityEpic = (
  action$
) =>
  action$
    .ofType(ADD_FEED_FULFILLED, RANK_ACTIVITY_FULFILLED)
    .debounceTime(100)
    .switchMap(action => {
      return ajaxAuth.getJSON(`/api/activities/byid/${action.activityId}`, { 'Content-Type': 'application/json' })
        .map(response => {
          return { type: FETCH_ACTIVITY_FULFILLED, payload: response }
        })
        .catch((err) => {
          console.log("Caught Error, continuing")
          console.log(err)

          //Return an empty Observable which gets collapsed in the output
          return Observable.of({ type: FETCH_ACTIVITY_REJECTED, payload: err });
        });
    });

export function getFeed({ id }) {
  return (dispatch) => {
    dispatch({ type: GET_FEED_PENDING, payload: id });
  }
}



export const getFeetEpic = (
  action$
) =>
  action$
    .ofType(GET_FEED_PENDING)
    .filter(action => action.payload.length > 2)
    .debounceTime(100)
    .switchMap(action => {
      return ajaxAuth
        .getJSON(`/api/activities/feed/${action.payload}`)
        .map(response => {
          return { type: GET_FEED_FULFILLED, payload: response }
        })
        .catch((err) => {
          console.log("Caught Error, continuing")
          //Return an empty Observable which gets collapsed in the output
          return Observable.of({ type: GET_FEED_REJECTED, payload: err });
        });
    });



export function addFavorite({ text, files }) {
  let data = new FormData();
  data.append('text', text);
  data.append('file', files);
  return (dispatch) => {
    dispatch({ type: ADD_FAVORITE_PENDING, payload: data });
  }
}

export const addFavoriteEpic = (
  action$
) =>
  action$
    .ofType(ADD_FAVORITE_PENDING)
    .debounceTime(100)
    .switchMap(action => {
      return ajaxAuth
        .post(`/api/activities/favorite/`, action.payload)
        .map(response => {
          return { type: ADD_FAVORITE_FULFILLED, payload: response }
        })
        .catch((err) => {
          console.log("Caught Error, continuing")
          //Return an empty Observable which gets collapsed in the output
          return Observable.of({ type: ADD_FAVORITE_REJECTED, payload: err });
        });
    });

export function getFavorite({ id }) {
  return (dispatch) => {
    dispatch({ type: ADD_FAVORITE_PENDING, payload: id });
  }
}

export const getFavoriteEpic = (
  action$
) =>
  action$
    .ofType(GET_FAVORITE_PENDING)
    .filter(action => action.payload.length > 2)
    .debounceTime(100)
    .switchMap(action => {
      return ajaxAuth
        .getJSON(`/api/activities/favorite/${action.payload}`)
        .map(response => {
          return { type: GET_FAVORITE_FULFILLED, payload: response }
        })
        .catch((err) => {
          console.log("Caught Error, continuing")
          //Return an empty Observable which gets collapsed in the output
          return Observable.of({ type: GET_FAVORITE_REJECTED, payload: err });
        });
    });