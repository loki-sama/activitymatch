
import { FILE_UPLOAD_PENDING, FILE_UPLOAD_REJECTED, FILE_UPLOAD_FULFILLED } from '../types'
import { Observable } from 'rxjs/Observable';
import ajaxAuth from '../../utils/ajaxAuth'


//activities


export function uploadFile({ file, name }) {
    let data = new FormData();
    data.append('file', file);
    data.append('name', name);
    return (dispatch) => {
        dispatch({ type: FILE_UPLOAD_PENDING, payload: data });
    }
}

export const uploadFileEpic = (
    action$
) =>
    action$
        .ofType(FILE_UPLOAD_PENDING)
        .debounceTime(100)
        .switchMap(action => {
            return ajaxAuth
                .post(`/api/images/upload/`, action.payload)
                .map(response => {
                    return { type: FILE_UPLOAD_FULFILLED, payload: response }
                })
                .catch((err) => {
                    console.log("Caught Error, continuing")
                    //Return an empty Observable which gets collapsed in the output
                    return Observable.of({ type: FILE_UPLOAD_REJECTED, payload: err });
                });
        });