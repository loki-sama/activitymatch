import api from "../../utils/api";
import { userLoggedIn } from "./authAction";
import axios from 'axios'
import setAuthorizationHeader from '../../utils/setAuthorizationHeader'

export const signup = data => dispatch =>
  api.user.signup(data).then(user => {

    localStorage.token = user.token;
    setAuthorizationHeader(user.token);
    
    dispatch(userLoggedIn(user));
  });

