import { FETCH_CATEGORIES_PENDING, FETCH_CATEGORIES_FULFILLED, FETCH_CATEGORIES_REJECTED } from '../types'
import axios from 'axios'

export function fetchCategories() {
    return function (dispatch) {
        dispatch({ type: FETCH_CATEGORIES_PENDING });

        axios.get("/api/categories")
            .then((response) => {
                dispatch({ type: FETCH_CATEGORIES_FULFILLED, payload: response.data })
            })
            .catch((err) => {
                dispatch({ type: FETCH_CATEGORIES_REJECTED, payload: err })
            })
    }
}