import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ItemPageView from "./ItemPageView";
import { fetchActivityById, addFeed, rankActivity, favActivity } from "../../../redux/actions/activityAction";
import { uploadFile } from "../../../redux/actions/uploadFileAction";

class ItemPage extends React.Component {
    componentDidMount() {
        this.props.fetchActivityById(this.props.match.params.activityid)
    }
    render() {
        const activity = this.props.nowActivity || this.props.activity
        const id = this.props.match.params.activityid
        const categoryId = this.props.match.params.categoryId
        return (
            <ItemPageView favorite={activity.getIn(['ranks', 0, 'favorite'])} 
            favActivity={(fav) => this.props.favActivity(this.props.rowIndex, activity.get('_id'), fav)} 
            rank={activity.getIn(['ranks', 0, 'rank'])} 
            rankActivity={(rank) => this.props.rankActivity(this.props.rowIndex, activity.get('_id'), rank)} 
            fetchActivity={() => this.props.fetchActivityById(this.props.match.params.activityid)} 
            activityId={id} 
            categoryId={categoryId} 
            activity={activity} 
            onFeedSubmit={(e) => this.props.addFeed(id, e)} 
            uploadFile={this.props.uploadFile}
            imageId={this.props.imageId}/>
        );
    }
}

ItemPage.propTypes = {

};

function mapStateToProps(state, ownProps) {
    return {
        activity: state.getIn(['activity', 'present', ownProps.match.params.categoryId, 'activities', ownProps.match.params.activityid]),
        nowActivity: state.getIn(['nowActivity']),
        imageId: state.getIn(['uploadFile', '_id'])
    }
}
export default connect(mapStateToProps, { fetchActivityById, addFeed, rankActivity, favActivity, uploadFile })(ItemPage);
