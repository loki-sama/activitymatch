import {
  Feed,
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment,
  activeItem,
  Menu,
  Icon
} from 'semantic-ui-react'
import React, { Component } from 'react';
import Validator from "validator";
import { UploadButton } from "../components/FileUpload";
export default class ItemPageView extends Component {
  state = {
    data: { kind: '0', message: '', resource: '' }, activeItem: 'all', selected: false,
    selected_image: {
      id: "",
      image: "",
      tags: "",
      webformatURL: ""
    }
  }

  onChange = (e) => {
    this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } })
  }
  handleChange = (e, { value }) => this.setState({ data: { ...this.state.data, ['kind']: value } })

  handleFeedSubmit = (e, data) => {
    this.props.onFeedSubmit({ ...this.state.data, ['resource']: this.props.imageId })
    this.setState({ data: { kind: '0', message: '', resource: '' } })
  }

  handleFileUpload = (file) => {
    if (file) {
      let reader = new FileReader();
      reader.onloadend = () => {
        this.setState({ selected_image: { ...this.state.selected_image, ['webformatURL']: reader.result }, selected: true })
      }
      reader.readAsDataURL(file)
      this.props.uploadFile({ file, name: file.name })
    }
  }
  filter = () => {
    let events = !!this.props.activity.get('feeds') ? this.props.activity.get('feeds').toJS() : []

    if (this.state.activeItem == 'first timer') {
      events = events.filter(value => (value.kind == 1))
    }
    else if (this.state.activeItem == 'guides') {
      events = events.filter(value => (value.kind == 2))
    }
    return events.map(value => ({
      date: value.createdAt, extraText: value.message, meta: '4 Likes',
      image: 'https://i2.wp.com/beebom.com/wp-content/uploads/2016/01/Reverse-Image-Search-Engines-Apps-And-Its-Uses-2016.jpg?w=640&ssl=1',
    }))
      .reverse()
  }
  handleItemClick = (e, { name }) => this.setState({ activeItem: name })
  render() {
    const { activeItem } = this.state
    const events = (!!this.props.activity.get('feeds') ?
      this.props.activity
        .get('feeds')
        .toJS()
        .map(value => ({
          date: value.createdAt, extraText: value.message, meta: '4 Likes',
          image: 'https://i2.wp.com/beebom.com/wp-content/uploads/2016/01/Reverse-Image-Search-Engines-Apps-And-Its-Uses-2016.jpg?w=640&ssl=1',
        }))
        .reverse()
      : [])

    const { kind } = this.state.data

    return (
      <Grid celled='internally'>
        <Grid.Row>
          <Grid.Column width={3}>
            <Menu icon='labeled' vertical>
            {/*  <Menu.Item name='favorite'
                active={activeItem === 'favorite'}
                onClick={(this.props.activity.getIn('ranks', 0, 'favorite') == 1) ? (() => this.props.favActivity(0)) : (() => this.props.favActivity(1))}>
                <Icon name='favorite' inverted={(this.props.activity.getIn('ranks', 0, 'favorite') == 0) ? (true) : (false)} />
                Favorite
              </Menu.Item>
            */}
              <Menu.Item name='like outline' active={activeItem === 'like outline'} onClick={() => this.props.rankActivity(1)}>
                <Icon name={(this.props.rank == 1) ? ('thumbs up') : ('thumbs outline up')} />
                Like
              </Menu.Item>

              <Menu.Item name='dislike outline' active={activeItem === 'dislike outline'} onClick={() => this.props.rankActivity(2)}>
                <Icon name={(this.props.rank == 2) ? ('thumbs down') : ('thumbs outline down')} />
                Dislike
              </Menu.Item>
            </Menu>
          </Grid.Column>
          <Grid.Column width={10}>
            <Header as='h1'>{this.props.activity.get('name')}</Header>
            <Segment.Group horizontal>
              <Segment>
                <Button style={{ background: 'transparent', height: '100%' }} floated='left' icon='chevron left' /></Segment>
              <Segment>
                <Image src={this.props.activity.get('image')} />
              </Segment>
              <Segment><Button style={{ background: 'transparent', height: '100%' }} floated='right' icon='chevron right' /></Segment>
            </Segment.Group>
            <p>{this.props.activity.get('description')}</p>
          </Grid.Column>
          <Grid.Column width={3}>
          </Grid.Column>
        </Grid.Row>

        <Grid.Row>
          <Grid.Column width={3}>
            <Image disabled={!this.state.selected} src={this.state.selected_image.webformatURL} />

        
          </Grid.Column>
          <Grid.Column width={10}>

            <Form reply onSubmit={this.handleFeedSubmit} >
              <Form.TextArea name='message' value={this.state.data.message} onChange={this.onChange} />
              <Form.Group inline>
                <label>Post</label>
                <Form.Radio label='Comment' value='0' checked={kind === '0'} onChange={this.handleChange} />
                <Form.Radio label='first timer' value='1' checked={kind === '1'} onChange={this.handleChange} />
                <Form.Radio label='guide' value='2' checked={kind === '2'} onChange={this.handleChange} />
              </Form.Group>
              <Button content='Add Post' labelPosition='left' icon='edit' />
              {/*
              <UploadButton label='Image' onUpload={this.handleFileUpload} />
              <Button content='Speak' labelPosition='left' icon='unmute' />
              */}
            </Form>
            <Menu pointing secondary>
              <Menu.Item name='all' active={activeItem === 'all'} onClick={this.handleItemClick} />
              <Menu.Item name='first timer' active={activeItem === 'first timer'} onClick={this.handleItemClick} />
              <Menu.Item name='guides' active={activeItem === 'guides'} onClick={this.handleItemClick} />
            </Menu>
            <Feed events={this.filter()} />
          </Grid.Column>
          <Grid.Column width={3}>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    );
  }
}