import {
  Button,
  Form,
  Grid,
  Header,
  Image,
  Message,
  Segment
} from 'semantic-ui-react'
import React, { Component } from 'react';
import Validator from "validator";
import { Link } from 'react-router-dom'

class LoginForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: {
        email: "",
        password: ""
      },
      loading: false,
      errors: {
        email: "",
        password: ""
      }
    };
  }
  onChange = (e) => {
    this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } })
  }

  onSubmit = (e) => {
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .catch(err =>
          this.setState({ errors: err.response.data.errors, loading: false })
        );
    }
  }

  /**
   * Validates form data
   */
  validate = data => {
    const errors = {};
    if (!Validator.isEmail(data.email)) errors.email = "Invalid email";
    if (!data.password) errors.password = "Can't be blank";
    return errors;
  };

  render() {

    const { data, errors, loading } = this.state;
    console.log(errors)
    return (
      <div className='login-form'>
        {/*
      Heads up! The styles below are necessary for thxe correct render of this example.
      You can do same with CSS, the main idea is that all the elements up to the `Grid`
      below must have a height of 100%.
    */}
        <style>
          {
            ` body > div,
            body > div > div,
            body > div > div > div.login-form {
              height: 100%;
            }`
          }
        </style>
        <Grid textAlign='center' style={{
          height: '100%'
        }} verticalAlign='middle'>
          <Grid.Column style={{
            maxWidth: 450
          }}>
            <Header as='h2' color='blue' textAlign='center'>
              <Image src='/logo.png' /> {' '}Log-in to your account
            </Header>
            <Form size='large' onSubmit={this.onSubmit}>
              <Segment stacked>
             
                <Form.Input fluid
                  name='email'
                  type="email"
                  id="email"
                  error={!!errors.email}
                  value={data.email}
                  onChange={this.onChange}
                  icon='user' iconPosition='left'
                  placeholder='Username, Handynummer oder E-mail Addresse' />

                <Form.Input fluid
                  type="password"
                  id="password"
                  name="password"
                  error={!!errors.password}
                  value={data.password}
                  onChange={this.onChange}
                  icon='lock'
                  iconPosition='left'
                  placeholder='Password' />

                <Button color='blue' fluid size='large'>Login</Button>
              </Segment>
            </Form>
            <Message>
              New to us? 
              <Link to='signup'>Sign Up</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default LoginForm
