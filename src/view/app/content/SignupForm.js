import React from 'react'
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'
import isEmail from "validator/lib/isEmail";
import PropTypes from "prop-types";
import { Link } from 'react-router-dom'

class SignupForm extends React.Component {
  state = {
    data: {
      email: "",
      password: ""
    },
    loading: false,
    errors: {}
  };

  onChange = e =>
    this.setState({
      ...this.state,
      data: { ...this.state.data, [e.target.name]: e.target.value }
    });

  onSubmit = e => {
    e.preventDefault();
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      this.setState({ loading: true });
      this.props
        .submit(this.state.data)
        .catch(err =>
          this.setState({ errors: err.response.data.errors, loading: false })
        );
    }
  };

  validate = data => {
    const errors = {};

    if (!isEmail(data.email)) errors.email = "Invalid email";
    if (!data.password) errors.password = "Can't be blank";

    return errors;
  };
  render() {
    const { data, errors, loading } = this.state;
    
    return (
      <div className='login-form' >
        {/*
      Heads up! The styles below are necessary for the correct render of this example.
      You can do same with CSS, the main idea is that all the elements up to the `Grid`
      below must have a height of 100%.
    */}
        < style > {`
      body > div,
      body > div > div,
      body > div > div > div.login-form {
        height: 100%;
      }
    `}</style >
        <Grid
          textAlign='center'
          style={{ height: '100%' }}
          verticalAlign='middle'
        >
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as='h2' color='blue' textAlign='center'>
              <Image src='/logo.png' />
              {' '}Sign-up a new account
        </Header>
            <Form size='large' onSubmit={this.onSubmit} loading={loading}>
              <Segment stacked>
                <Form.Input
                  fluid
                  type="email"
                  id="email"
                  name="email"
                  icon='user'
                  iconPosition='left'
                  placeholder='Handynummer oder E-mail Addresse'
                  value={data.email}
                  onChange={this.onChange}
                />

                <Form.Input
                  fluid
                  type="password"
                  id="password"
                  name="password"
                  icon='lock'
                  iconPosition='left'
                  placeholder='Password'
                  value={data.password}
                  onChange={this.onChange}
                />

                <Button color='blue' fluid size='large'>Sign-up</Button>
              </Segment>
            </Form>
            <Message>
              Already have an account? <Link to='login'>Sign In</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div >
    )
  }
}

SignupForm.propTypes = {
  submit: PropTypes.func.isRequired
};

export default SignupForm;
