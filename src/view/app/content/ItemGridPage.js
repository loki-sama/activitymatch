import React, { Component } from 'react'
import { Divider, Grid, Button, Menu, Icon, Container, Visibility } from 'semantic-ui-react'
import Row from './itemGrid/Row';
import { PropTypes } from 'prop-types'
import { connect } from 'react-redux'
import _ from 'lodash'
import AddItem from './itemGrid/AddItem';
import { addActivity, searchImage, searchWikipedia } from '../../../redux/actions/activityAction'
import { uploadFile } from '../../../redux/actions/uploadFileAction'

const overlayStyle = {
  margin: '0em 1em 1em 0em',
}

const fixedOverlayStyle = {
  ...overlayStyle,
  position: 'fixed',
  bottom: '30px',
  zIndex: 10,
  right: 0
}

const fixedOverlayMenuStyle = {
  position: 'relative',
  transition: 'left 0.5s ease',
}

class ItemGridPage extends Component {
  stickOverlay = () => this.setState({ overlayFixed: true })
  unStickOverlay = () => this.setState({ overlayFixed: false })
  state = {
    menuFixed: false,
    overlayFixed: false,
  }
  transformCategories = (category) => Object.keys(category).map(
    (value) => ({ key: value, value: value, text: category[value].category })
  )
  transformWikipediaToSearchResult = (wikipediaData) => {
    if (!!wikipediaData.pageResult) {
      const validKeyMap = {
        name: 'title',
        mainImage: 'image',
        description: 'description',
      }
      const result = Object.keys(wikipediaData.pageResult || {}).reduce(
        (output, key) => {
          if (!!validKeyMap[key]) output[validKeyMap[key]] = wikipediaData.pageResult[key]
          return output
        }, {}
      )
      return [result]
    }
    else if (!!wikipediaData.searchResult) {
      return wikipediaData.searchResult.map(value => ({ title: value }))
    }
    return []
  }
  render() {
    const { menuFixed, overlayFixed, overlayRect } = this.state

    let range = n => [...Array(n).keys()]
    const categoryid = this.props.match.params.categoryid || false
    const categories = this.props.categories || false
    let grid = <div />
    var addMenu = (
      <Container text>

        <Visibility
          offset={80}
          once={false}
          onTopPassed={this.stickOverlay}
          onTopVisible={this.unStickOverlay}
          style={{ ...overlayStyle, ...overlayRect }}
        />

        <div
          ref={this.handleOverlayRef}
          style={fixedOverlayStyle}
        >
          <Menu
            icon='labeled'
            style={fixedOverlayMenuStyle}
            vertical
          >
            <Menu.Item>
              <AddItem
                uploadFile={this.props.uploadFile}
                category={this.transformCategories(this.props.categories.toJS() || {})}
                images={this.props.images}
                wikipedia={this.transformWikipediaToSearchResult(this.props.wikipedia)}
                submit={(data) => (this.props.addActivity(data))}
                searchImage={this.props.searchImage}
                searchWikipedia={this.props.searchWikipedia} />
            </Menu.Item>
          </Menu>
        </div>
      </Container>
    );

    if (!categoryid) {
      if (!!categories) {
        var listItems = categories.map((value, index) => (
          <Row rowIndex={index} quantityItems={this.props.quantityItems} categoryName={value.get('category')} />
        ))

        grid = (
          <Grid centered columns={this.props.quantityItems + 1} >
            {listItems}
            {addMenu}
          </Grid>
        );
      }

    } else {
      grid = (
        <Grid centered columns={this.props.quantityItems + 1} >
          <Divider horizontal>{categoryid}</Divider>
          {range(10).map((value, index) => (<Row key={categoryid + index} rowIndex={categoryid + index} quantityItems={this.props.quantityItems} />))}
          {addMenu}
        </Grid>
      );
    }
    return grid
  }
}
ItemGridPage.propTypes = {
  quantityItems: PropTypes.number.isRequired
};

function mapStateToProps(state) {
  return {
    categories: state.get('category'),
    quantityItems: state.getIn(['layout', 'quantityItems']),
    images: state.getIn(['image']).toJS(),
    wikipedia: state.getIn(['wikipedia']).toJS()
  };
}

export default connect(mapStateToProps, { addActivity, searchImage, searchWikipedia, uploadFile })(ItemGridPage);