import React, { Component } from 'react'
import {
  Button,
  Image,
  Grid,
  Loader,
  Segment,
  Label,
  Icon,
  Popup
} from 'semantic-ui-react'
import { Link } from 'react-router-dom'

const style = {
  width: 260,
  height: 150,
  overflow: 'hidden',
}
class RowItem extends Component {
  state = { image: false }
  onImageError = () => {
    this.setState({ image: 'https://cdn.pixabay.com/photo/2017/02/12/21/29/false-2061131_1280.png' })
  }
  render() {
    let props = this.props
    return (<Grid.Column stretched className='overlay_wrapper'>
      {
        props.loading ? (
          <Segment style={style} loading>
          </Segment>
        ) :
          (
            <Popup
              key={props.id}
              trigger={(
                <div>
                  <Link to={'/activity/' + props.rowIndex + "/" + props.id}>
                    <Image onError={this.onImageError} style={style} src={this.state.image || props.image} />
                  </Link>
                  <div className='overlay_description'>
                    <div className='overlay_description_content'>
                      <Button.Group floated='right'>
                        <Button compact icon={(props.rank == 1) ? ('thumbs up') : ('thumbs outline up')} onClick={() => props.rankActivity(1)} />
                        <Button compact icon={(props.rank == 2) ? ('thumbs down') : ('thumbs outline down')} onClick={() => props.rankActivity(2)} />
                      </Button.Group>
                    </div>
                  </div>
                </div>
              )}
              header={props.activity.get('name')}
              content={props.activity.get('description').substr(0, 300) + '...'}
            />
          )
      }

    </Grid.Column>
    );
  }
}
export default RowItem
