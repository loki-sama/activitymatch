import React, { Component } from 'react'
import { Grid, Button, Divider } from 'semantic-ui-react'
import RowItem from './Row/RowItem';
import { searchImage, fetchActivities, rankActivity } from '../../../../redux/actions/activityAction'
import { PropTypes } from 'prop-types'
import { connect } from 'react-redux'
import Immutable from 'immutable'
import { ActionCreators as UndoActionCreators } from 'redux-undo-immutable'

class Row extends Component {
    componentWillMount() {
        if (!this.props.activities) {
            this.props.fetchActivities(this.props.rowIndex, this.props.quantityItems)
        }
    }

    render() {
        let range = n => [...Array(n).keys()]
        const { fetching, fetched, activities, onUndo, canUndo, canRedo, onRedo, rowIndex } = this.props

        const style = { width: 40, margin: 0 }
        if (!fetched) {
            var listItems = range(this.props.quantityItems).map(
                (activity, i) => <Grid.Column key={rowIndex + i}><RowItem loading={true} /></Grid.Column>
            );
        } else {
            var listItems = activities.map(
                (activity, i) =>
                    <Grid.Column key={rowIndex + i}>
                        <RowItem id={i} width={14 / this.props.quantityItems} rank={activity.getIn(['ranks', 0, 'rank'])} rankActivity={(rank) => this.props.rankActivity(this.props.rowIndex, i, rank)} loading={false} image={activity.get('image')} activity={activity} rowIndex={rowIndex} />
                    </Grid.Column>
            );
        }
        return (
            <Grid.Row id={rowIndex} key={rowIndex} >
                <Divider horizontal>{this.props.categoryName}</Divider>

                <Grid.Column width={1} style={style} key={rowIndex + '-1'}>
                    <Button onClick={onUndo} disabled={!canUndo} style={{ background: 'transparent', height: '100%' }} floated='left' icon='chevron left' />
                </Grid.Column>
                {listItems}
                <Grid.Column width={1} style={style} key={rowIndex + '-2'}>
                    <Button style={{ background: 'transparent', height: '100%' }} floated='right' onClick={canRedo ? onRedo : () => this.props.fetchActivities(this.props.rowIndex, this.props.quantityItems)} floated='right' icon='chevron right' />
                </Grid.Column>
            </Grid.Row>
        )

    }
}

Row.propTypes = {
    fetchActivities: PropTypes.func.isRequired,
    rankActivity: PropTypes.func.isRequired,
    //activities: PropTypes.instanceOf(Immutable.List).isRequired
};


let onUndo = () => (dispatch) => dispatch(UndoActionCreators.undo())
let onRedo = () => (dispatch) => dispatch(UndoActionCreators.redo())


const mapDispatchToProps = {
    onUndo,
    fetchActivities,
    rankActivity,
    onRedo,

}




function mapStateToProps(state, ownProps) {
    return {
        canUndo: state.getIn(['activity', 'past']).size > 0,
        canRedo: state.getIn(['activity', 'future']).size > 0,
        activities: state.getIn(['activity', 'present', ownProps.rowIndex, 'activities']),
        fetching: state.getIn(['activity', 'present', ownProps.rowIndex, 'fetching']),
        fetched: state.getIn(['activity', 'present', ownProps.rowIndex, 'fetched']),
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Row);