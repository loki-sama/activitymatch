import React, { Component } from 'react'
import { Button, Header, Icon, Image, Modal, Form, Checkbox, Search, Dropdown, Label } from 'semantic-ui-react'

class AddItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                name: "",
                image: "",
                category: "",
                description: "",
                tags: "",
            },
            selected: false,
            selected_image: {
                id: "",
                image: "",
                tags: "",
                webformatURL: ""
            }
        }
    }

    onSearch = (e) => {
        this.onChange(e)
        this.props.searchImage(this.state.data[e.target.name])
    }
    onWikipediaSearch = (e) => {
        this.onChange(e)
        this.props.searchWikipedia(e.target.value || '')
    }
    onWikipediaResultSelect = (e, { result }) => {
        this.setState({ selected_image: { ...this.state.selected_image, ['webformatURL']: result.image }, selected: true })
        this.setState({ data: { ...this.state.data, ['name']: result.title, ['description']: result.description } })
    }
    onResultSelect = (e, { result }) => {
        this.setState({ selected_image: result, selected: true })

    }
    onDropdownChange = (e, { name, value }) => {
        this.setState({ data: { ...this.state.data, [name]: value } })

    }

    onChange = (e) => {
        this.setState({ data: { ...this.state.data, [e.target.name]: e.target.value } })
        console.log(this.state.data);
    }

    onSubmit = (e) => {
        //const errors = this.validate(this.state.data);
        const errors = {}
        this.setState({ errors });
        if (Object.keys(errors).length === 0) {
            this.setState({ loading: true });
            let data = {
                name: this.state.data.name,
                image: this.state.selected_image.webformatURL,
                category: this.state.data.category,
                description: this.state.data.description,
                tags: this.state.data.tags,
            }
            this.props.submit(data)
        }
    }



    handleFileUpload = (e) => {
        const file = e.target.files[0]
        let reader = new FileReader();
        reader.onloadend = () => {
            this.setState({ selected_image: { ...this.state.selected_image, ['webformatURL']: reader.result }, selected: true })
        }

        reader.readAsDataURL(file)
        this.props.uploadFile({ file, name: file.name })
    }
    render() {
        return (
            <Modal trigger={<div><Icon name='plus square' />
                <br />Add</div>
            }>
                <Modal.Header>Add a new activity</Modal.Header>
                <Modal.Content image>
                    <Form onSubmit={this.onSubmit}>
                        <Form.Field>
                            <label>Image</label>
                            <Search
                                value={this.state.data.image}
                                results={this.props.images}
                                onSearchChange={this.onSearch}
                                onResultSelect={this.onResultSelect}
                                type='search'
                                name='image'
                                placeholder='Upload or search Image' />
                            <Label pointing>Node: Search pixabay for image to used as primary image.</Label>
                            <Search
                                value={this.state.data.wikipedia}
                                results={this.props.wikipedia}
                                onSearchChange={this.onWikipediaSearch}
                                onResultSelect={this.onWikipediaResultSelect}
                                type='search'
                                name='wikipedia'
                                placeholder='Add wikipage or search wikipedia' />
                            <Label pointing>Node: Search wikipedia with api. Use return data to fill picture name and description fields.
                            </Label>
                            {/*<input type="file" id="file1" name="files1" onChange={this.handleFileUpload} autocomplete="off" /> 
                            <Label pointing>Node: This is not yet working. Adds image to server but is not used. </Label>
                            */}
                            <Image disabled={!this.state.selected} src={this.state.selected_image.webformatURL} />
                        </Form.Field>
                        <Form.Field>
                            <label>Name</label>
                            <input
                                onChange={this.onChange}
                                value={this.state.data.name}
                                name='name'
                                placeholder='The Name of the activity' />
                        </Form.Field>
                      
                        <Form.Field>
                            <label>Categories</label>

                            <Dropdown placeholder='The Category of the activity'
                                search selection
                                options={this.props.category}
                                value={this.state.data.category}
                                onChange={this.onDropdownChange}
                                id='category'
                                name='category'
                                className='category'
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Description</label>
                            <input
                                onChange={this.onChange}
                                value={this.state.data.description}
                                name='description'
                                placeholder='A short description of the activity' />
                        </Form.Field>
                        {/*
                        <Form.Field>
                            <label>Tags</label>
                            <input onChange={this.onChange}
                                value={this.state.data.tags}
                                id='tags'
                                name='tags'
                                placeholder='Tags' />
                        </Form.Field>
                        */}
                    </Form>
                </Modal.Content>

                <Modal.Actions>

                    <Button primary onClick={this.onSubmit}>
                        Proceed <Icon name='right chevron' />
                    </Button>
                </Modal.Actions>


            </Modal >
        )
    }
}

export default AddItem