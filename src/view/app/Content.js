import React from 'react'
import {Container, Segment} from 'semantic-ui-react'

const Content = (props) => (
  <Segment style={{
    padding: '3em 0em'
  }} vertical>
    <Container textAlign='center'>
      {props.children}
    </Container>
  </Segment>
)

export default Content
