import React from 'react'
import {
  Button,
  Menu,
  Input,
  Segment,
  Dropdown,
  Image,
} from 'semantic-ui-react'
import PropTypes from "prop-types";
import { logout } from '../../../redux/actions/authAction'
import { connect } from "react-redux";
import { withRouter, Link } from 'react-router-dom'

const style = {
  background: '#f7f7f7',
  borderRadius: 0,
  boxShadow: 'none',
  transition: 'box-shadow 0.5s ease, padding 0.5s ease'
}


const Navbar = ({ isAuthenticated, logout }) => (
  <Segment textAlign='center' style={{
    padding: '0em 0em',
    border: '0em 0em',
    margin: 0
  }}>
    <Menu secondary size='massive' style={style}>
      <Menu.Item as={Link} to='/' header>
        <Image size='mini' src={require('../../../static/images/logo.svg')} />
      </Menu.Item>
      <Menu.Menu position='left'>
      {isAuthenticated &&
        <Dropdown item simple text='Categories'>
          <Dropdown.Menu>
            
            <Dropdown.Item as={Link} to='/activity'>All</Dropdown.Item>
            <Dropdown.Item as={Link} to='/category/Favorites'>Favorites</Dropdown.Item>
            <Dropdown.Divider />
            <Dropdown.Item as={Link} to='/category/Jobs'>Jobs</Dropdown.Item>
            <Dropdown.Item as={Link} to='/category/Sport'>Sport</Dropdown.Item>
            <Dropdown.Item as={Link} to='/category/Suggestions'>Suggestions</Dropdown.Item>
            <Dropdown.Item as={Link} to='/category/Activity'>Activity</Dropdown.Item>
            <Dropdown.Item as={Link} to='/category/Study'>Study</Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      }
        <Menu.Item>

          
          {/*
          <Input className='search' icon='search' placeholder='Search...' />
          */}
        </Menu.Item>
      </Menu.Menu>

      <Menu.Menu position='right'>
        {isAuthenticated &&
          <Menu.Item content='User' icon='user' as={Link} to='/' className='item' />
        }
        {isAuthenticated ? (
          <Menu.Item content='Log out'  icon='log out' className='item' as={Link} to='/login' onClick={() => logout()}  />

         
        ) : (
            <Menu.Item content='Log in' className='item'as={Link} to='/login' />
          )}
        {!isAuthenticated &&
          <Menu.Item content='Sign up' className='item' as={Link} to='/signup' />
           
        }
      </Menu.Menu>
    </Menu>
  </Segment>

)
Navbar.propTypes = {
  isAuthenticated: PropTypes.bool.isRequired,
  logout: PropTypes.func.isRequired
};

function mapStateToProps(state) {
  return {
    isAuthenticated: !!state.get('user').get('token')
  };
}

export default withRouter(connect(mapStateToProps, { logout: logout })(Navbar));
