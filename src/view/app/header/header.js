/*class Header extends Component {
  render() {
    return (
      <div className="hdSpace" role="header" data-reactid="3">
        <div id="hdPinTarget" className="akira-header scrollingHeader" data-reactid="4">
          <a aria-label="Netflix" className="logo icon-logoUpdate" href="/" data-reactid="5"></a>
          <ul role="navigation">
            <li className="browse active hasSubMenu">
              <a role="button" aria-haspopup="true" tabindex="0">
                <!-- react-text: 57 -->Browse
                <!-- /react-text -->
              </a>
              <span className="caret" role="presentation"></span>
            </li>
          </ul>
          <div className="secondary-nav">
            <div className="nav-element">
              <div className="searchBox">
                <button className="searchTab" tabindex="0" aria-label="Search">
                  <span className="icon-search"></span>
                  <span className="label">Search</span>
                </button>
              </div>
            </div>
            <div className="nav-element">
              <span className="notifications">
                <button className="notifications-menu" aria-haspopup="true" aria-label="Notifications">
                  <span className="icon-button-notification"></span>
                  <span className="notification-pill">8</span>
                </button>
              </span>
            </div>
            <div className="nav-element">
              <div className="account-menu-item">
                <div className="account-dropdown-button" role="button" tabindex="0" aria-haspopup="true" aria-label="Timo - Account &amp; Settings">
                  <Link to="/YourAccount">
                    <span className="profile-link" role="presentation">
                      <img className="profile-icon" src="https://secure.netflix.com/ffe/profiles/avatars_v2/32x32/PICON_027.png" alt="">
                        <span className="profile-name">Timo</span>
                      </span>
                    </a>
                    <span className="caret" role="presentation"></span>
                  </div>
                </div>
              </div>
            </div>
            <!-- react-empty: 6 -->
          </div>
        </div>
