import React, { Component } from 'react';
//import Titel from './components/Title';
import Header from './app/Header';
import Content from './app/Content';
import PropTypes from "prop-types";
import { updateDimensions } from '../redux/actions/layout'
import { fetchCategories } from '../redux/actions/categoryAction'

import { connect } from "react-redux";
import { withRouter } from 'react-router-dom'
class App extends Component {
  /**
 * Add event listener
 */
  componentDidMount() {
    this.props.updateDimensions();
    this.props.fetchCategories();
    window.addEventListener("resize", this.props.updateDimensions);

  }

  /**
   * Remove event listener
   */
  componentWillUnmount() {
    window.removeEventListener("resize", this.props.updateDimensions);
  }

  render() {
    return (
      <div className="App">
        <Header />
        {console.log(this.props)}
        <Content>{this.props.children}</Content>
      </div>
    );
  }
}
App.propTypes = {
  updateDimensions: PropTypes.func.isRequired
};


export default withRouter(connect(null, { updateDimensions: updateDimensions, fetchCategories: fetchCategories })(App));
