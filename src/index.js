import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux"
import dotenv from "dotenv";
import App from './view/App';
import registerServiceWorker from './registerServiceWorker';
import GuestRoute from "./routes/Guest";
import UserRoute from "./routes/User";
import '../node_modules/semantic-ui-css/semantic.min.css';
import './static/styles/App.css';
import './static/styles/index.css';
import axios from "axios";

import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import HomePage from './view/app/content/HomePage';

import ItemGridPage from './view/app/content/ItemGridPage';
import LoginPage from './view/app/content/LoginPage';
import SignupPage from './view/app/content/SignupPage';
import ItemPage from './view/app/content/ItemPage';
import setAuthorizationHeader from './utils/setAuthorizationHeader'

import store from "./redux/store"
import decode from "jwt-decode";
import { userLoggedIn } from "./redux/actions/authAction";

dotenv.config();

axios.defaults.baseURL = process.env.NODE_ENV=='production' ? process.env.REACT_APP_API_URL_PRODUCTION : process.env.REACT_APP_API_URL_DEVELOPMENT;

if (localStorage.token) {
  const payload = decode(localStorage.token);
  const user = {
    token: localStorage.token,
    email: payload.email,
    confirmed: payload.confirmed
  };
  setAuthorizationHeader(localStorage.token);
  store.dispatch(userLoggedIn(user));
}
ReactDOM.render((
  <Provider store={store}>
    <Router>
      <App>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <UserRoute exact path="/activity" component={ItemGridPage} />
          <UserRoute path="/category/:categoryid" component={ItemGridPage} />
          <GuestRoute path="/login" component={LoginPage} />
          <GuestRoute path="/signup" component={SignupPage} />
          <UserRoute path="/activity/:categoryId/:activityid" component={ItemPage} />
        </Switch>
      </App>
    </Router>
  </Provider>
), document.getElementById('view'));

registerServiceWorker();
