import axios from "axios";

export default {
  user: { 
    login: credentials =>
      axios.post("/api/auth", { credentials }).then(res => res.data.user),
    logout: callback =>
      axios.post("/api/auth/logout" ).then(callback()),
    signup: user =>
      axios.post("/api/users", { user }).then(res => res.data.user),
    confirm: token =>
      axios
        .post("/api/auth/confirmation", { token })
        .then(res => res.data.user),
    resetPasswordRequest: email =>
      axios.post("/api/auth/reset_password_request", { email }),
    validateToken: token => axios.post("/api/auth/validate_token", { token }),
    resetPassword: data => axios.post("/api/auth/reset_password", { data })
  },
  activities: {
    get: (parameters) => 
      axios.post("/api/activities", { parameters}).then(res => res.data.activities),
    rank: (id, rank) => 
      axios.post("/api/activities/rank", { id, rank}),
    add: (data) => 
      axios.post("/api/activities/add", {data})

  }
};
