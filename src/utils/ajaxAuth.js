import { ajax as ajaxRxjs } from 'rxjs/observable/dom/ajax';

const defaultHeaders = {
    authorization: `Bearer ${localStorage.token}`
};


const apiBaseUrl = process.env.NODE_ENV == 'production' ? process.env.REACT_APP_API_URL_PRODUCTION : process.env.REACT_APP_API_URL_DEVELOPMENT;

export default class ajaxAuth {
    defaultHeaders = localStorage.token === undefined ? {
        authorization: `Bearer ${localStorage.token}`
    } : {}
    static get = (url, headers) =>
        ajaxRxjs.get(apiBaseUrl + url, Object.assign({}, defaultHeaders, headers));

    static getJSON = (url, headers) =>
        ajaxRxjs.getJSON(apiBaseUrl + url, Object.assign({}, defaultHeaders, headers));

    static post = (url, body, headers) =>
        ajaxRxjs.post(apiBaseUrl + url, body, Object.assign({}, defaultHeaders, headers));

}
